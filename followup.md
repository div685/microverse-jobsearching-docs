Job Searching - Job application follow-ups

Use this document to keep track of the number of follow-up emails you send each week. 

Instructions: 
Write down the period you’re tracking. 
Highlight in the color of your choice, how many follow-up emails you sent this week.  

Example: 
From the (Insert date) to the (insert date) 
I have sent 10 follow-up emails:

1 · 2 · 3 · 4 · 5 · 6 · 7 · 8 · 9 · 10 · 11 · 12 · 13 . 14 · 15 · 16 · 17 · 18 · 19 · 20 · 21· 22 · 23 · 24 · 25 · 26 · 27 · 28 · 29 · 30 · 31· 32 · 33 · 34+
 
Now, choose your color and let us know how many you sent this week, Then repeat each week to track your progress over time.

---

From the (Insert date) to the (insert date) 
I have sent X follow-up emails:

1 · 2 · 3 · 4 · 5 · 6 · 7 · 8 · 9 · 10 · 11 · 12 · 13 . 14 · 15 · 16 · 17 · 18 · 19 · 20 · 21· 22 · 23 · 24 · 25 · 26 · 27 · 28 · 29 · 30 · 31· 32 · 33 · 34+ 


