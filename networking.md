Job Searching - Continue building your network - Networking Tracker
Building a network is a continuous work and requires time and energy. This template will help you define and register the networking activities that you do each day.
Section 1.
Review the list below of potential tasks to do to grow your network. These are all ideas that would be beneficial actions, but you can also consider creating your own networking activity and doing that.  
   - [ ] Sharing that you are looking for a job with existing contacts.
    1. Draft a message and sharing it on a platform - LinkedIn, Twitter, Slack

    2. Draft a personal message and sharing it with relevant people

	  Refresh your list of dream jobs. Are there any new companies you might want to target?
    1. You can make a copy of this template and collate a list if it doesn’t exist.

  - [ ] Identify relevant new contacts in the dream companies who you want to connect with.

  - [ ] Reach out to new contacts on Twitter.
Twitter Networking: How to use Twitter to network your way into your dream job

  - [ ] Reach out to new contacts on LinkedIn.
LinkedIn Networking: Video - How to network with others on LinkedIn

  - [ ] Reach out to new contacts on Slack.
Slack Networking: How to use Slack Channels to meet recruiters and find a job

  - [ ] Attend a meetup event or networking event.
  - [ ] Come up with your own networking activity.

Section 2.
Add two actions from the list above to your networking tracker below, including details about the activity and what you did (like where you went to a meet-up or who you reached out to) and complete them.You can continue using the same tracker here throughout your job search, and simply submit this same Google Doc each time you do this exercise. That way, you can track all your progress in one place. 
Networking Tracker

|Date  		| Networking Activity 		| Outcomes			|
|-------------- | ------------------- 		| ----------------------- 	|
|Aug 22, 2022	| Reached out to the person on linkedIn and Twitter | The Person whom I reached out on LinkedIn accepted my request and the person on Twitter did not reply |	
|Aug 23, 2022 	| Reached out to the person on Slack about the job | Waiting for the person to reply |
|Aug 24, 2022	| send connection request to the HR | waiting for them to accepts |
|Aug 25, 2022	|  | |
|Aug 26, 2022	| Reached out the person on Discord and Slack | The person sent me link to apply for the job |


