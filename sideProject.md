Side Project Planning

Project Type - Solo

Solo means you are building something on your own from your own idea. 
Group means you are working on an original idea together with another person or group of people. If a group of people from Microverse are working together on this, you can all share this same planning document, and then each of you submits the same link in your project submission form. If this is a group project, list the other people you intend to collaborate with.
Open Source means you have found a project online that you would like to contribute to significantly. If you are choosing an open-source project, you will want to ensure you are working on other small things to ensure you can commit daily to keep your GitHub green! 

> Project Description - Activity Tracker,



Solo or Group side project

Project home - Share the GitHub URL of the project. It’s okay if you don’t have this created immediately when you turn this in, but you can update this doc later to include the project links.

Who are your users? When putting together a project, it’s always important to think about who you are building it for. Write that out here. Is this a chrome plugin for other Microverse students? Is this an npm module for other developers? Tell us who would be using your project.

Core features & deadlines 
What are the core features you plan to implement? And when do you plan to finish implementing each feature? Complete the table below listing at least 2-3 core features you plan to work on in order to launch an MVP version of this project by the end of this month that you can add to your Portfolio.

|Feature description | date by when feature should be done |
| ------ | ------ |
|Example: Core feature 1 | 11 Sep 2022 | 
|Example: Core feature 2 | 25 Sep 2022 |
|Example: Core feature 3 | 31 Sep 2022 |







